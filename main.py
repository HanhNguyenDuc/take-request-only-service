from urllib import response
from flask import Flask, jsonify, request
import logging
import os
logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
app = Flask(__name__)

@app.route('/json_to_success', methods=["POST"])
def json_to_success():
    json_data = request.json
    logging.info(f"json_to_success | request: {json_data}")
    response_data = {
        "status": 0,
        "msg": "Success",
    }
    logging.info(f"json_to_success | response: {response_data}")
    return jsonify(response_data)


@app.route('/json_to_fail', methods=["POST"])
def json_to_fail():
    json_data = request.json
    logging.info(f"json_to_fail | request: {json_data}")
    response_data = {
        "status": -1,
        "msg": "Something wrong",
    }
    logging.info(f"json_to_fail | response: {response_data}")
    return jsonify(response_data)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8222)